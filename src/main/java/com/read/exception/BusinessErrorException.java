package com.read.exception;

import com.read.common.BusinessMsgEnum;
import lombok.Data;

/**
 * @date
 * @desc 自定义异常
 */
@Data
public class BusinessErrorException extends RuntimeException {

    /**
     * 异常码
     */
    private Integer code;
    /**
     * 异常提示信息
     */
    private String message;

    public BusinessErrorException(BusinessMsgEnum businessMsgEnum) {
        this.code = businessMsgEnum.getCode();
        this.message = businessMsgEnum.getMsg();
    }


}
