package com.read.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
/**
 * 柱状图 饼状图
 */
public class BarVo {

    private String barbecue_name;
    private Integer barbecue_num;
}
