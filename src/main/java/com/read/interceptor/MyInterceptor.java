package com.read.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @date 2022/10/26
 * @desc
 */
@Slf4j    // 加了这个注解  可以直接使用  log 对象输出日志
public class MyInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        /**
         * 使用场景： 如果某些接口需要登录后才能访问  可以在这里做登录校验
         *
         * */

        System.out.println(request.getRequestURI());  // 输出请求路径

        if (handler instanceof HandlerMethod) {
            // 如果接口方法处理器  就会走这里
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            Method method = handlerMethod.getMethod();
            String methodName = method.getName();
            log.info("====拦截到了方法：{}，在该方法执行之前执行====", methodName);
        }
        // 如果是 资源处理器  比如 ResourceHttpRequestHandler  就不走上面的if

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

        System.out.println("后置拦截，执行了controller 中的方法之后 响应视图之前 会执行这个方法");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("整个请求执行完毕 可以做一些收尾工作，如果请求过程中出现异常 这里也可以得到异常信息");
    }
}
