package com.read.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashSet;

/**
 * @version 1.0
 * <p>
 * 登录拦截器
 */
public class LoginInterceptor implements HandlerInterceptor {

    //设置一些白名单，放行的访问路径，资源
    private HashSet<String> whiteUrls = new HashSet<>();

    public LoginInterceptor() {
        //添加不拦截的路径
        whiteUrls.add("/index.html");
        whiteUrls.add("/login.html");
        whiteUrls.add("/doc.html");
    }

    //访问目标方法前执行
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        //获取当前访问的路径
        String requestURI = request.getRequestURI();//访问：http://localhost:8080/login.html -> 返回：/login.html
        //String requestURL  = request.getRequestURL().toString();//访问什么路径，就返回什么路径

        //遍历
        for (String url : whiteUrls) {
            //判断请求的路径是否有白名单中的字符串
            if (requestURI.contains(url)) {
                return true;//放行
            }
        }

        //再判断，当代码走到这，意味着访问的路径不是白名单中放行的路径，考虑当前用户是否登录过了
        if (request.getSession().getAttribute("admin") != null) {
            return true;
        } else {
            //没有登录成功，跳转到登录页面
            response.sendRedirect(request.getContextPath() + "/login.html");
            return false;
        }
    }
}
