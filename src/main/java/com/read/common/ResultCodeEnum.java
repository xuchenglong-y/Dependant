package com.read.common;


import lombok.Getter;

/**
 * @date
 * @desc
 */
@Getter
public enum ResultCodeEnum {
    //枚举值
    SUCCESS(true, "操作成功", 200),
    ERROR(false, "操作失败", 444),
    BAD_SQL_GRAMMAR(false, "sql语法错误", 445),
    MISSING_PARAM(false, "缺少请求参数", 446),
    NULL_POINT(false, "空指针异常", 447),
    UNKNOW_RESAON(false, "未知错误", 999);

    private Boolean success;

    private String message;

    private Integer code;

    ResultCodeEnum(Boolean success, String message, Integer code) {
        this.success = success;
        this.message = message;
        this.code = code;
    }
}
