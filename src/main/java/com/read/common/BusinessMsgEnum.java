package com.read.common;

import lombok.Getter;
import lombok.Setter;

/**
 * @date
 * @desc 跟 自定义异常 搭配使用的 枚举类
 */
@Getter
public enum BusinessMsgEnum {

    /**
     * 参数异常
     */
    PARMETER_EXCEPTION(102, "参数异常!"),
    /**
     * 等待超时
     */
    SERVICE_TIME_OUT(103, "服务调用超时！"),
    /**
     * 参数过大
     */
    PARMETER_BIG_EXCEPTION(102, "输入的图片数量不能超过50张!"),
    /**
     * 500 : 一劳永逸的提示也可以在这定义
     */
    UNEXPECTED_EXCEPTION(500, "系统发生异常，请联系管理员！");
    // 还可以定义更多的业务异常
    /**
     * 消息码
     */
    private Integer code;
    /**
     * 消息内容
     */
    private String msg;

    private BusinessMsgEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }


}
