package com.read.common;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @date
 * @desc 简单的封装一个向前端响应的json 数据结构
 */
@Data
public class R {

    private Integer code;   // 响应的状态码


    private String message;  // 响应的信息


    private Boolean success;  // 是否成功


    private Map<Object, Object> data = new HashMap<>();

    private R() {

    }

    private R(Integer code, String message, Boolean success) {
        this.code = code;
        this.message = message;
        this.success = success;
    }


    //返回成功

    public static R ok() {
        R r = new R(ResultCodeEnum.SUCCESS.getCode(), ResultCodeEnum.SUCCESS.getMessage(), ResultCodeEnum.SUCCESS.getSuccess());
        return r;
    }

    //返回失败
    public static R error() {
        R r = new R(ResultCodeEnum.ERROR.getCode(), ResultCodeEnum.ERROR.getMessage(), ResultCodeEnum.ERROR.getSuccess());
        return r;
    }


    public static R setResult(ResultCodeEnum resultCodeEnum) {
        R r = new R();
        r.setSuccess(resultCodeEnum.getSuccess());
        r.setMessage(resultCodeEnum.getMessage());
        r.setCode(resultCodeEnum.getCode());
        return r;
    }


    public R success(Boolean success) {
        this.setSuccess(success);
        return this;
    }


    public R message(String message) {
        this.setMessage(message);
        return this;
    }

    public R code(Integer code) {
        this.setCode(code);
        return this;
    }


    public R data(Object key, Object value) {
        this.data.put(key, value);
        return this;
    }

    public R data(Map<Object, Object> map) {
        this.setData(map);
        return this;
    }

}
