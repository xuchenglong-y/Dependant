package com.read.service;

import com.read.pojo.StudentLeave;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xcl
 * @since 2022-12-11
 */
public interface StudentLeaveService extends IService<StudentLeave> {

}
