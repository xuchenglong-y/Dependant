package com.read.service;

import com.read.pojo.Teacher;

import java.util.List;

public interface TeachersService {
    List<Teacher> findAll();
}
