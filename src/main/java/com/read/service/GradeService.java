package com.read.service;

import com.read.pojo.GradeTable;

import java.util.List;

public interface GradeService {
    List<GradeTable> findAll();
}
