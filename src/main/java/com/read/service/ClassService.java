package com.read.service;

import com.read.pojo.ClassTable;

import java.util.List;

public interface ClassService {
    //查询全部记录
    List<ClassTable> findClass();

    //消课课时
    List<ClassTable> findtype();

    //已购课时
    List<ClassTable> findtypes();
}
