package com.read.service;

import com.read.pojo.MessageTable;

import java.util.List;

public interface MessageTableService {
    //查询全部
    List<MessageTable> findAll();

    //更新未读消息
    int update(MessageTable messageTable);
}
