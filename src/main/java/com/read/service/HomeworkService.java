package com.read.service;

import com.read.pojo.Homework;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HomeworkService {

    List<Homework> findBytime();

    List<Homework> findAll();
}
