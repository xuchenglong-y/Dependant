package com.read.service;

import com.read.pojo.Works;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
public interface WorksService extends IService<Works> {

    List<Works> findWorks(Integer userId, Integer worksType, Integer worksState);
}
