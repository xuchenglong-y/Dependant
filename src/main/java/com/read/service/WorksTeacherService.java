package com.read.service;

import com.read.pojo.Teacher;
import com.read.pojo.WorksTeacher;

import java.util.List;

public interface WorksTeacherService {
    List<WorksTeacher> findbytors(String tors);
}
