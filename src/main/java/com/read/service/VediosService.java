package com.read.service;

import com.read.pojo.Vedios;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xcl
 * @since 2022-12-13
 */
public interface VediosService extends IService<Vedios> {

}
