package com.read.service;

import com.read.pojo.WinterGrade;

import java.util.List;

public interface WinterGradeService {
    List<WinterGrade> findAll();
}
