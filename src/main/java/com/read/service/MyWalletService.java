package com.read.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.read.pojo.MyWallet;


/**
 * 
 *
 * @author xcl
 * @email 2364639157@qq.com
 * @date 2022-12-10 16:57:43
 */
public interface MyWalletService extends IService<MyWallet> {

}

