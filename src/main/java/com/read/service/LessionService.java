package com.read.service;

import com.read.pojo.Lession;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface LessionService {
    //查询全部精选好课
    List<Lession> findAll();
}
