package com.read.service;

import com.read.common.R;
import com.read.pojo.UserMmaterials;

public interface UserMaterialsService {

    UserMmaterials findbymobile(String mobile);//根据手机号查询个人信息

    int changeName (UserMmaterials userMmaterials);//修改个人信息

    String verificationCode (String mobile);//获取验证码

    int adduser(String code,String mobile);//添加用户

    int updatepassword(String mobile,String password);//修改密码

    int updatepayment(String mobile,String payment);//修改、设置支付密码

    int code(String code, String mobile);//验证验证码

    UserMmaterials findbypassword(String mobile,String password);//登录

    int deletemobile(String mobile,String code);//逻辑删除







}
