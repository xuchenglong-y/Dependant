package com.read.service;

import com.read.pojo.Journalism;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xcl
 * @since 2022-12-18
 */
public interface JournalismService extends IService<Journalism> {

}
