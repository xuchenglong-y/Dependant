package com.read.service;

import com.read.pojo.Preview;
import com.baomidou.mybatisplus.extension.service.IService;
import com.read.pojo.Works;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
public interface PreviewService extends IService<Preview> {
    List<Preview> findAll(Integer userId);
}
