package com.read.service.impl;

import com.read.pojo.Teacher;
import com.read.mapper.TeacherMapper;
import com.read.service.TeacherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xcl
 * @since 2022-12-18
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements TeacherService {
    @Resource
    private TeacherMapper teacherMapper;


    @Override
    public List<Teacher> findAll(Integer userId) {
        return teacherMapper.findAll(userId);
    }
}
