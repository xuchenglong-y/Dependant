package com.read.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.read.mapper.WorksTeacherMapper;
import com.read.pojo.Teacher;
import com.read.pojo.UserMmaterials;
import com.read.pojo.WorksTeacher;
import com.read.service.WorksTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class WorksTeacherServiceImpl implements WorksTeacherService {

    @Autowired
    private WorksTeacherMapper worksTeacherMapper;
    @Override
    public List<WorksTeacher> findbytors(String tors) {
        QueryWrapper<WorksTeacher> wrapper = new QueryWrapper<WorksTeacher>();
        wrapper.eq("tors", tors);

        List<WorksTeacher> worksTeachers = worksTeacherMapper.selectList(wrapper);

        return worksTeachers;
    }
}
