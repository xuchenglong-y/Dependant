package com.read.service.impl;

import com.read.mapper.GradeMapper;
import com.read.pojo.GradeTable;
import com.read.service.GradeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class GradeServiceImpl implements GradeService {
    @Resource
    private GradeMapper gradeMapper;

    @Override
    public List<GradeTable> findAll() {
        return gradeMapper.findAll();
    }
}
