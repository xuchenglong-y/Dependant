package com.read.service.impl;

import com.read.mapper.TeachersMapper;
import com.read.pojo.Teacher;
import com.read.service.TeachersService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class TeachersServiceImpl implements TeachersService {
    @Resource
    private TeachersMapper teachersMapper;

    @Override
    public List<Teacher> findAll() {
        return teachersMapper.findAll();
    }
}
