package com.read.service.impl;

import com.read.pojo.StudentLeave;
import com.read.mapper.StudentLeaveMapper;
import com.read.service.StudentLeaveService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xcl
 * @since 2022-12-11
 */
@Service
public class StudentLeaveServiceImpl extends ServiceImpl<StudentLeaveMapper, StudentLeave> implements StudentLeaveService {

}
