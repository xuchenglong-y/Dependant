package com.read.service.impl;

import com.read.mapper.MessageTableMapper;
import com.read.pojo.MessageTable;
import com.read.service.MessageTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xcl
 * @since 2022-12-10
 */
@Service
public class MessageTableServiceImpl implements MessageTableService {
    @Resource
    private MessageTableMapper messageTableMapper;

    public List<MessageTable> findAll(){
        return messageTableMapper.findAll();
    }

    @Override
    public int update(MessageTable messageTable) {
        return messageTableMapper.update(messageTable);
    }

}
