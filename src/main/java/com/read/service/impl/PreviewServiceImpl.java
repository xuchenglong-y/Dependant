package com.read.service.impl;

import com.read.pojo.Preview;
import com.read.mapper.PreviewMapper;
import com.read.service.PreviewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
@Service
public class PreviewServiceImpl extends ServiceImpl<PreviewMapper, Preview> implements PreviewService {
    @Resource
    private PreviewMapper previewMapper;

    @Override
    public List<Preview> findAll(Integer userId) {
        return previewMapper.findAll(userId);
    }
}
