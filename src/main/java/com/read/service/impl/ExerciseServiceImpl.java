package com.read.service.impl;

import com.read.mapper.ExerciseMapper;
import com.read.pojo.Exercise;
import com.read.service.ExerciseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ExerciseServiceImpl implements ExerciseService {
    @Resource
    private ExerciseMapper exerciseMapper;
    @Override
    public List<Exercise> findAll() {
        return exerciseMapper.findAll();
    }
}
