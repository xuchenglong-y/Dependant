package com.read.service.impl;


import com.read.mapper.HomeworkMapper;
import com.read.pojo.Homework;
import com.read.service.HomeworkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class HomeworkServiceImpl implements HomeworkService {
    @Resource
    private HomeworkMapper homeworkMapper;

    @Override
    public List<Homework> findBytime() {
        return homeworkMapper.findBytime();
    }

    @Override
    public List<Homework> findAll() {
        return homeworkMapper.findAll();
    }
}
