package com.read.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.read.mapper.UserMaterialsMapper;
import com.read.pojo.UserMmaterials;
import com.read.service.UserMaterialsService;
import com.read.utils.SendSmsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Service
public class UsermaterialsServiceImpl implements UserMaterialsService {
    @Autowired
    private UserMaterialsMapper userMaterialsMapper;

    @Resource
    private StringRedisTemplate sgr;

    @Override
    public UserMmaterials findbymobile(String mobile) {
        QueryWrapper<UserMmaterials> wrapper = new QueryWrapper<UserMmaterials>();
        wrapper.eq("mobile", mobile);

        UserMmaterials userMmaterials = userMaterialsMapper.selectOne(wrapper);
        return userMmaterials;
    }

    @Override
    public int changeName(UserMmaterials userMmaterials) {

        QueryWrapper<UserMmaterials> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", userMmaterials.getMobile());

        int i = userMaterialsMapper.update(userMmaterials, wrapper);


        return i;
    }

    @Override
    public String verificationCode(String mobile) {

        System.out.println(mobile);

        String s = SendSmsUtil.sendCheckCode(mobile);

        sgr.opsForValue().set(mobile, s,120, TimeUnit.SECONDS);

        return s;
    }

    @Override
    public int adduser(String code,String mobile) {
        String s = sgr.opsForValue().get(mobile);
        int i = 0;
        if (s.equals(code)){
            UserMmaterials userMmaterials = new UserMmaterials();
            userMmaterials.setAvatar("hangzhou.aliyuncs.com/9461d7caly1h6rwbw8bsnj20u013zgtu.jpg");
            userMmaterials.setDel(0);
            userMmaterials.setVersion(1);
            userMmaterials.setMobile(mobile);
            i = userMaterialsMapper.insert(userMmaterials);
        }
        return i;
    }

    @Override
    public int updatepassword(String mobile,String password) {

            QueryWrapper<UserMmaterials> wrapper = new QueryWrapper<>();
            wrapper.eq("mobile", mobile);
            UserMmaterials userMmaterials = new UserMmaterials();
            userMmaterials.setMobile(mobile);
            userMmaterials.setPassword(password);
        int i = userMaterialsMapper.update(userMmaterials,wrapper);


        return i;
    }

    @Override
    public int updatepayment(String mobile,String payment) {

        QueryWrapper<UserMmaterials> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        UserMmaterials userMmaterials = new UserMmaterials();
        userMmaterials.setMobile(mobile);
        userMmaterials.setPassword(payment);
            int i = userMaterialsMapper.update(userMmaterials,wrapper);

        return i;

    }

    @Override
    public int code(String code, String mobile) {
        String s = sgr.opsForValue().get(mobile);

        if (s.equals(code)){
            return 1;
        }else {
            return 0;
        }


    }

    @Override
    public UserMmaterials findbypassword(String mobile, String password) {

        QueryWrapper<UserMmaterials> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);

        UserMmaterials userMmaterials = userMaterialsMapper.selectOne(wrapper);

        if (password.equals(userMmaterials.getPassword())){
            return userMmaterials;
        }else {
            return null;
        }

    }

    @Override
    public int deletemobile(String mobile, String code) {
        String s = sgr.opsForValue().get(mobile);
        QueryWrapper<UserMmaterials> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        int i=0;
        if (s.equals(code)){
            i = userMaterialsMapper.delete(wrapper);
        }

        return i;
    }


}
