package com.read.service.impl;

import com.read.service.MyWalletService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.read.mapper.MyWalletMapper;
import com.read.pojo.MyWallet;


@Service("myWalletService")
public class MyWalletServiceImpl extends ServiceImpl<MyWalletMapper, MyWallet> implements MyWalletService {

}