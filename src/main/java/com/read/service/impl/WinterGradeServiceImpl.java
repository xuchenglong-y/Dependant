package com.read.service.impl;

import com.read.mapper.WinterGradeMapper;
import com.read.pojo.WinterGrade;
import com.read.service.WinterGradeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class WinterGradeServiceImpl implements WinterGradeService {
    @Resource
    private WinterGradeMapper winterGradeMapper;
    @Override
    public List<WinterGrade> findAll() {
        return winterGradeMapper.findAll();
    }
}
