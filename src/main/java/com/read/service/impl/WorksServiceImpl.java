package com.read.service.impl;

import com.read.pojo.Works;
import com.read.mapper.WorksMapper;
import com.read.service.WorksService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
@Service
public class WorksServiceImpl extends ServiceImpl<WorksMapper, Works> implements WorksService {

    @Resource
    private WorksMapper worksMapper;


    @Override
    public List<Works> findWorks(Integer userId, Integer worksType, Integer worksState) {
        return worksMapper.findWorks(userId, worksType, worksState);
    }
}
