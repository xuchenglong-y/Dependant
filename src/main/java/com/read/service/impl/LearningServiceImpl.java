package com.read.service.impl;

import com.read.pojo.Learning;
import com.read.mapper.LearningMapper;
import com.read.service.LearningService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xcl
 * @since 2022-12-16
 */
@Service
public class LearningServiceImpl extends ServiceImpl<LearningMapper, Learning> implements LearningService {

}
