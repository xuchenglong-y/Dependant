package com.read.service.impl;

import com.read.mapper.VocieMapper;
import com.read.pojo.Voice;
import com.read.service.VoiceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class VoiceServiceImpl implements VoiceService {
    @Resource
    private VocieMapper vocieMapper;
    @Override
    public List<Voice> findtype1() {
        return vocieMapper.findtype1();
    }

    @Override
    public List<Voice> findtype2() {
        return vocieMapper.findtype2();
    }

    @Override
    public List<Voice> findtype3() {
        return vocieMapper.findtype3();
    }

    @Override
    public Boolean delete(int vId) {
        return vocieMapper.delete(vId);
    }
}
