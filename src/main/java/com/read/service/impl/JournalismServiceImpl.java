package com.read.service.impl;

import com.read.pojo.Journalism;
import com.read.mapper.JournalismMapper;
import com.read.service.JournalismService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xcl
 * @since 2022-12-18
 */
@Service
public class JournalismServiceImpl extends ServiceImpl<JournalismMapper, Journalism> implements JournalismService {

}
