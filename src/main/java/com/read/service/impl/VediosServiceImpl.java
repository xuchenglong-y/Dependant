package com.read.service.impl;

import com.read.pojo.Vedios;
import com.read.mapper.VediosMapper;
import com.read.service.VediosService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xcl
 * @since 2022-12-13
 */
@Service
public class VediosServiceImpl extends ServiceImpl<VediosMapper, Vedios> implements VediosService {

}
