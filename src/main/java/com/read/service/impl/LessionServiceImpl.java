package com.read.service.impl;

import com.read.mapper.LessionMapper;
import com.read.pojo.Lession;
import com.read.service.LessionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class LessionServiceImpl implements LessionService {
    @Resource
    private LessionMapper lessionMapper;

    @Override
    public List<Lession> findAll() {
        return lessionMapper.findAll();
    }
}
