package com.read.service.impl;

import com.read.mapper.WorksMapper;
import com.read.pojo.GoodLession;
import com.read.mapper.GoodLessionMapper;
import com.read.pojo.Works;
import com.read.service.GoodLessionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
@Service
public class GoodLessionServiceImpl extends ServiceImpl<GoodLessionMapper, GoodLession> implements GoodLessionService {
    @Resource
    private GoodLessionMapper goodLessionMapper;


    @Override
    public List<GoodLession> findAll(Integer userId) {
        return goodLessionMapper.findAll(userId);
    }
}
