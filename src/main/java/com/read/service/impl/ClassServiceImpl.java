package com.read.service.impl;

import com.read.mapper.ClassMapper;
import com.read.pojo.ClassTable;
import com.read.service.ClassService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class ClassServiceImpl implements ClassService {
    @Resource
    private ClassMapper classMapper;

    @Override
    public List<ClassTable> findClass() {
        return classMapper.findClass();
    }

    @Override
    public List<ClassTable> findtype() {
        return classMapper.findtype();
    }

    @Override
    public List<ClassTable> findtypes() {
        return classMapper.findtypes();
    }
}
