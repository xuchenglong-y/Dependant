package com.read.service.impl;

import com.read.pojo.Integral;
import com.read.mapper.IntegralMapper;
import com.read.service.IntegralService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author xcl
 * @since 2022-12-16
 */
@Service
public class IntegralServiceImpl extends ServiceImpl<IntegralMapper, Integral> implements IntegralService {

}
