package com.read.service;

import com.read.pojo.Voice;

import java.util.List;

public interface VoiceService {
    //查询入学测试
    List<Voice> findtype1();
    //查询名师陪练
    List<Voice> findtype2();
    //查询课文陪读
    List<Voice> findtype3();

    Boolean delete(int vId);
}
