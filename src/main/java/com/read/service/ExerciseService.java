package com.read.service;

import com.read.pojo.Exercise;

import java.util.List;

public interface ExerciseService {
    List<Exercise> findAll();
}
