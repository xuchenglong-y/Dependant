package com.read.service;

import com.read.pojo.Integral;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xcl
 * @since 2022-12-16
 */
public interface IntegralService extends IService<Integral> {

}
