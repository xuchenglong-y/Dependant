package com.read.service;

import com.read.pojo.Teacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.read.pojo.Works;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xcl
 * @since 2022-12-18
 */
public interface TeacherService extends IService<Teacher> {
    List<Teacher> findAll(Integer userId);
}
