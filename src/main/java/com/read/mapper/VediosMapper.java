package com.read.mapper;

import com.read.pojo.Vedios;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xcl
 * @since 2022-12-13
 */
public interface VediosMapper extends BaseMapper<Vedios> {

}
