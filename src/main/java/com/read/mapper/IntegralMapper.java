package com.read.mapper;

import com.read.pojo.Integral;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xcl
 * @since 2022-12-16
 */
public interface IntegralMapper extends BaseMapper<Integral> {

}
