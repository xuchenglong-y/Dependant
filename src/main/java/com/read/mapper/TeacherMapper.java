package com.read.mapper;

import com.read.pojo.Teacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.read.pojo.Works;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xcl
 * @since 2022-12-18
 */
public interface TeacherMapper extends BaseMapper<Teacher> {
    List<Teacher> findAll(Integer userId) ;
}
