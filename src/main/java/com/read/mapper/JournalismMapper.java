package com.read.mapper;

import com.read.pojo.Journalism;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xcl
 * @since 2022-12-18
 */
public interface JournalismMapper extends BaseMapper<Journalism> {

}
