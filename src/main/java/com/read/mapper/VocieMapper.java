package com.read.mapper;

import com.read.pojo.Voice;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VocieMapper {
    //查询入学测试
    List<Voice> findtype1();
    //查询名师陪练
    List<Voice> findtype2();
    //查询课文陪读
    List<Voice> findtype3();
    //通过Id删除
    Boolean delete(int vId);
}
