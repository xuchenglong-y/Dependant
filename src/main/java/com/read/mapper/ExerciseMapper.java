package com.read.mapper;

import com.read.pojo.Exercise;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExerciseMapper {
    List<Exercise> findAll();
}
