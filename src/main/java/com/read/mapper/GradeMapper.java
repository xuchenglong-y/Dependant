package com.read.mapper;

import com.read.pojo.GradeTable;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface GradeMapper {
    List<GradeTable> findAll();
}
