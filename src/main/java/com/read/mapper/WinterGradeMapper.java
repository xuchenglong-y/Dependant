package com.read.mapper;

import com.read.pojo.WinterGrade;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WinterGradeMapper {
    List<WinterGrade> findAll();
}
