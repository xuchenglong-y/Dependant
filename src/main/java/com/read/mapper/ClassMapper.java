package com.read.mapper;

import com.read.pojo.ClassTable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClassMapper {
    //全部记录
    List<ClassTable> findClass();

    //消课课时
    List<ClassTable> findtype();

    //已购课时
    List<ClassTable> findtypes();
}
