package com.read.mapper;

import com.read.pojo.Teacher;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeachersMapper {
    List<Teacher> findAll();
}
