package com.read.mapper;

import com.read.pojo.StudentLeave;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xcl
 * @since 2022-12-11
 */
public interface StudentLeaveMapper extends BaseMapper<StudentLeave> {

}
