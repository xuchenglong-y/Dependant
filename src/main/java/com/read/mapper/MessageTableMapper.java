package com.read.mapper;

import com.read.pojo.MessageTable;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xcl
 * @since 2022-12-10
 */
@Repository
public interface MessageTableMapper {
    //查询全部

    List<MessageTable> findAll();

    //更新未读消息
    int update(MessageTable messageTable);
}
