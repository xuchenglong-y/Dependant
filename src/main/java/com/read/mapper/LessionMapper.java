package com.read.mapper;

import com.read.pojo.Lession;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LessionMapper {
    //查询全部精选好课
    List<Lession> findAll();
}
