package com.read.mapper;

import com.read.pojo.Works;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
@Repository
public interface WorksMapper extends BaseMapper<Works> {

       List<Works> findWorks(Integer userId, Integer worksType, Integer worksState) ;

}
