package com.read.mapper;

import com.read.pojo.MyWallet;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 
 * 
 * @author xcl
 * @email 2364639157@qq.com
 * @date 2022-12-10 16:57:43
 */
@Repository
public interface MyWalletMapper extends BaseMapper<MyWallet> {


}
