package com.read.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.read.pojo.WorksTeacher;

public interface WorksTeacherMapper extends BaseMapper<WorksTeacher> {
}
