package com.read.mapper;

import com.read.pojo.GoodLession;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.read.pojo.Works;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
public interface GoodLessionMapper extends BaseMapper<GoodLession> {
    List<GoodLession> findAll(Integer userId) ;
}
