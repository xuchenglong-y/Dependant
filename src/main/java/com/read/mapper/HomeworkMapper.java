package com.read.mapper;

import com.read.pojo.Homework;

import java.util.List;

public interface HomeworkMapper {

    List<Homework> findBytime();

    List<Homework> findAll();
}
