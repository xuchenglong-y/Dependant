package com.read.controller;

import com.read.common.R;
import com.read.config.AliyunOssUtil;
import com.read.pojo.UserMmaterials;
import com.read.service.impl.UsermaterialsServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;

@RestController
@RequestMapping("usermaterials")
@Api(tags = "我的_设置_个人资料_王正智")
public class UserMaterialsController {

    @Autowired
    private UsermaterialsServiceImpl usermaterialsService;

    @Autowired
    private AliyunOssUtil ossUtil;

    String upload = null;
    String uploadurl = null;


    @GetMapping("findbymobile")
    @ApiOperation("根据账号手机号查找用户个人信息")
    @ApiImplicitParam(name = "mobile",value = "手机号，账号",required = true)
    public UserMmaterials findbymobile(@RequestParam(value = "mobile") String mobile) {

        return usermaterialsService.findbymobile(mobile);

    }


    @PostMapping("changeName")
    @ApiOperation("根据手机号修改昵称、性别、照片头像，通用接口")
    @ApiImplicitParam(name = "mobile",value = "手机号，账号",required = true)
    public R changeName(UserMmaterials userMmaterials){

        int i = usermaterialsService.changeName(userMmaterials);

        if (i>0){
            return R.ok();
        }else {
            return R.error();
        }

    }


}
