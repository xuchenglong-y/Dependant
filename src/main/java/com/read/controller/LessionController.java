package com.read.controller;

import com.read.pojo.Lession;
import com.read.service.LessionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/lession")
@Api(tags = "首页_精选好课__郭秉航")
public class LessionController {
    @Resource
    private LessionService lessionService;

    @ApiOperation("查询全部精选好课")
    @GetMapping("/findAll")
    public List<Lession> findAll() {
        return lessionService.findAll();
    }
}
