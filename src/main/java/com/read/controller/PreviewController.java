package com.read.controller;


import com.read.common.CommonResult;
import com.read.mapper.PreviewMapper;
import com.read.pojo.Preview;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
@RestController
@RequestMapping("/preview")
@Api(tags = "我的_收藏_预习_李继承")
public class PreviewController {
    @Resource
    private PreviewMapper previewMapper;

    @ApiOperation("查询全部") //测试完毕
    @GetMapping("/findAll")
    public CommonResult findAll(Integer userId){
        if( userId == null){
            return CommonResult.failed("参数异常");
        }else {

            List<Preview> preview = previewMapper.findAll(userId);

            return CommonResult.success(preview);
        }
    }

    /**
     * id 查id
     * @param id
     * @return
     */
    @ApiOperation("根据id查询信息")//测试完毕
    @GetMapping("findById")
    @ApiImplicitParam(name = "id",value = "预习id",required = true)
    public CommonResult findById(@RequestParam(value = "id") Integer id){
        if(id == null){
           return  CommonResult.failed("参数异常");
        }else {

        Preview preview = previewMapper.selectById(id);

        return CommonResult.success(preview);
        }
    }

    /**
     *
     * @param id
     * @return
     */
    @ApiOperation("根据id删除视频信息")//测试完毕
    @GetMapping("/deletedById")
    @ApiImplicitParam(name = "id",value = "预习tid",required = true)
    public CommonResult deletedById(@RequestParam(value = "id") Integer  id){
        if (id == null){
           return  CommonResult.failed("参数异常");
        }else {

            int i = previewMapper.deleteById(id);
            return CommonResult.success(i);
        }
    }

    @ApiOperation("批量删除")//测试完毕
    @GetMapping("/deletedByIds")

    public CommonResult deletedByIds(List<String> ids){

        int i = previewMapper.deleteBatchIds(ids);

        return CommonResult.success(i);
    }

}

