package com.read.controller;

import com.read.common.R;
import com.read.pojo.UserMmaterials;
import com.read.service.impl.UsermaterialsServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("user")
@Api(tags = "我的_设置_王正智")
public class UserMaterialsControllerTwo {

    @Resource
    private UsermaterialsServiceImpl usermaterialsService;


    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @PostMapping("verificationCode")
    @ApiOperation("发送验证码")
//    @ApiImplicitParam(name = "mobile", value = "手机号，账号", required = true)
    public R verificationCode(String mobile) {

//        String s = usermaterialsService.verificationCode(mobile);
        System.out.println("mobile" + mobile);
        String code = String.valueOf(getRandomNum());
        stringRedisTemplate.opsForValue().set(mobile, code,120, TimeUnit.SECONDS);
        if (code != null) {
            return R.ok().data("code", code); //发送完验证码返回给前端
        } else {
            return R.error();
        }

    }

    public static int getRandomNum() {

        Random r = new Random();

        return r.nextInt(900000) + 100000;//(Math.random()*(999999-100000)+100000)

    }

    @PostMapping("adduser")
    @ApiOperation("注册账号")
    @ApiImplicitParams({@ApiImplicitParam(name = "code", value = "验证码", required = true),
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true)})
    public R addUser(String code, String mobile) {

        int adduser = usermaterialsService.adduser(code, mobile);

        if (adduser > 0) {
            return R.ok();
        } else {
            return R.error();
        }
    }


    @PostMapping("updatepassword")
    @ApiOperation("根据手机号，修改密码，忘记密码通用")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true),
            @ApiImplicitParam(name = "password", value = "密码", required = true)})
    public R updatepassword(String mobile, String password) {

        int updatepassword = usermaterialsService.updatepassword(mobile, password);

        if (updatepassword > 0) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @PostMapping("updatepayment")
    @ApiOperation("根据手机号，修改、设置支付密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true),
            @ApiImplicitParam(name = "payment", value = "支付密码", required = true)})
    public R updatepayment(String mobile, String payment) {

        int updatepayment = usermaterialsService.updatepayment(mobile, payment);

        if (updatepayment > 0) {
            return R.ok();
        } else {
            return R.error();
        }
    }


    @PostMapping("codee")
    @ApiOperation("验证码是否正确")
    @ApiImplicitParams({@ApiImplicitParam(name = "code", value = "验证码", required = true),
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true)})
    public R codee(String code, String mobile) {

        int codee = usermaterialsService.code(code, mobile);

        if (codee > 0) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @PostMapping("loginpassword")
    @ApiOperation("登录")
    @ApiImplicitParams({@ApiImplicitParam(name = "password", value = "密码", required = true),
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true)})
    public R loginn(String mobile, String password) {

        UserMmaterials findbypassword = usermaterialsService.findbypassword(mobile, password);

        if (findbypassword != null) {
            return R.ok();
        } else {
            return R.error();
        }
    }
    @PostMapping("del")
    @ApiOperation("注销账号")
    @ApiImplicitParams({@ApiImplicitParam(name = "code", value = "验证码", required = true),
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true)})

    public R deletemobile(String mobile, String code) {
        int deletemobile = usermaterialsService.deletemobile(mobile, code);
        if (deletemobile>0){
            return R.ok();
        }else {
            return R.error();
        }

    }

}
