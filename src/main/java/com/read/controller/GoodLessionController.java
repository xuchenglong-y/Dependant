package com.read.controller;


import com.read.common.CommonResult;
import com.read.mapper.GoodLessionMapper;
import com.read.pojo.GoodLession;
import com.read.service.GoodLessionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
@RestController
@RequestMapping("/good-lession")
@Api(tags="我的_我的收藏_网课_李继承")
public class GoodLessionController {
    @Resource
    private GoodLessionMapper goodLessionMapper;
    @Resource
    private GoodLessionService goodLessionService;

    @ApiOperation("查询全部") //测试完毕
    @GetMapping("/findAll")
    public CommonResult findAll(Integer userId){
        if(userId == null){
            return CommonResult.failed("参数异常");
        }else {

            List<GoodLession> goodLessions = goodLessionService.findAll(userId);
            System.out.println(goodLessions);

            return CommonResult.success(goodLessions);
        }
    }


    /**
     * id 查id
     * @param gId
     * @return
     */
    @ApiOperation("根据id查询信息")//测试完毕
    @GetMapping("findById")
    @ApiImplicitParam(name = "gId",value = "网课gId",required = true)
    public CommonResult findById(@RequestParam(value = "gId") Integer gId){
        if (gId == null){
            return  CommonResult.failed("参数异常");
        }else {

            GoodLession goodLession = goodLessionMapper.selectById(gId);

            return CommonResult.success(goodLession);
        }
    }

    /**
     *
     * @param gId
     * @return
     */
    @ApiOperation("根据id删除视频信息")//测试完毕
    @GetMapping("/deletedById")
    @ApiImplicitParam(name = "id",value = "网课=gId",required = true)
    public CommonResult deletedById(@RequestParam(value = "gId") Integer  gId){

        int i = goodLessionMapper.deleteById(gId);
        return CommonResult.success(i);
    }

    @ApiOperation("批量删除")//测试完毕
    @GetMapping("/deletedByIds")

    public CommonResult deletedByIds(List<String>  gId){

        int i = goodLessionMapper.deleteBatchIds( gId);

        return CommonResult.success(i);
    }


}

