package com.read.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.read.common.R;
import com.read.pojo.Vedios;
import com.read.service.VediosService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xcl
 * @since 2022-12-13
 */
@RestController
@RequestMapping("/vedios")
@Api(tags = "视频_徐成龙")
public class VediosController {

    @Autowired
    private VediosService vediosService;

    @GetMapping("/findAllRecommend")
    @ApiOperation("视频_推荐_综合")
    public R findAllVideos() {
        List<Vedios> findAllVideos = vediosService.list();
        if (findAllVideos != null && findAllVideos.size() > 0) {
            return R.ok().data("findAllVideos", findAllVideos);
        } else {
            return R.error();
        }
    }

    @GetMapping("/findAllLight")
    @ApiOperation("视频_推荐_星光闪闪亮")
    public R findAllLight() {
        QueryWrapper<Vedios> vediosQueryWrapper = new QueryWrapper<>();
        vediosQueryWrapper.like("introduction", "星光闪闪亮");
        List<Vedios> vediosLightList = vediosService.list(vediosQueryWrapper);
        if (vediosLightList != null && vediosLightList.size() > 0) {

            return R.ok().data("vediosLightList", vediosLightList);
        } else {
            return R.error();
        }
    }

    @GetMapping("/findAllWrite")
    @ApiOperation("视频_推荐_云朗读")
    public R findAllWrite() {
        QueryWrapper<Vedios> vediosQueryWrapper = new QueryWrapper<>();
        vediosQueryWrapper.like("introduction", "蓝话筒云朗读");
        List<Vedios> findAllWrite = vediosService.list(vediosQueryWrapper);
        if (findAllWrite != null && findAllWrite.size() > 0) {
            return R.ok().data("findAllWrite", findAllWrite);
        } else {
            return R.error();
        }
    }

    @GetMapping("/findAllClass")
    @ApiOperation("视频_推荐_筒筒网课")
    public R findAllClass() {
        QueryWrapper<Vedios> vediosQueryWrapper = new QueryWrapper<>();
        vediosQueryWrapper.like("introduction", "筒筒网课");
        List<Vedios> findAllClass = vediosService.list(vediosQueryWrapper);
        if (findAllClass != null && findAllClass.size() > 0) {
            return R.ok().data("findAllClass", findAllClass);
        } else {
            return R.error();
        }
    }

    @PostMapping("/insert")
    @ApiOperation("视频_上传视频&音频及其对象信息")
    public R uploadVideo(@RequestBody Vedios videos) {
        if (videos != null) {
            boolean save = vediosService.save(videos);
            if (save) {
                return R.ok();
            } else {
                return R.error();
            }
        } else {
            return R.error();
        }
    }
}

