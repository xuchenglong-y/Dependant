package com.read.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.read.common.R;
import com.read.pojo.StudentLeave;
import com.read.service.StudentLeaveService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xcl
 * @since 2022-12-11
 */
@RestController
@RequestMapping("/studentLeave")
@Api(tags = "我的_我要请假_徐成龙")
public class StudentLeaveController {

    @Autowired
    private StudentLeaveService studentLeaveService;

    @PostMapping("/apply")
    @ApiOperation("我要请假_请假申请(需将用户UserID传入)")
    public R apply(@RequestBody StudentLeave studentLeave) {
        if (studentLeave != null) {
            boolean save = studentLeaveService.save(studentLeave);
            if (save) {
                return R.ok();
            } else {
                return R.error();
            }
        } else {
            return R.error();
        }
    }

    @GetMapping("/record")
    @ApiOperation("我的_我要请假_记录")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true)
    public R findStudentLeaveRecord(@RequestParam(value = "userId") Integer userId) {
        QueryWrapper<StudentLeave> studentLeaveQueryWrapper = new QueryWrapper<>();
        studentLeaveQueryWrapper.eq("user_id", userId);
        if (userId != null && userId > 0) {
            List<StudentLeave> studentLeaveList = studentLeaveService.list(studentLeaveQueryWrapper);
            if (studentLeaveList != null && studentLeaveList.size()>0) {
                return R.ok().data("studentLeaveList", studentLeaveList);
            } else {
                return R.error();
            }
        } else {
            return R.error();
        }
    }
}

