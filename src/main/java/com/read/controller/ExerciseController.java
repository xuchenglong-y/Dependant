package com.read.controller;

import com.read.pojo.Exercise;
import com.read.service.ExerciseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/exercise")
@Api(tags = "首页_名师陪练__郭秉航")
public class ExerciseController {
    @Resource
    private ExerciseService exerciseService;

    @ApiOperation("查询全部记录")
    @GetMapping("/findAll")
    public List<Exercise> findAll(){
        return exerciseService.findAll();
    }
}
