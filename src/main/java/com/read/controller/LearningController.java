package com.read.controller;


import com.read.common.CommonResult;
import com.read.mapper.LearningMapper;
import com.read.pojo.Learning;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xcl
 * @since 2022-12-16
 */
@RestController
@RequestMapping("/learning")
@Api(tags = "我的-我的学习记录-李继承")
public class LearningController {
    @Resource
    private LearningMapper learningMapper;

    @ApiOperation("查询全部")
    @GetMapping("findAll")
    public CommonResult findAll(){
        List<Learning> list = learningMapper.selectList(null);
        if (list == null){
            return CommonResult.failed("查询有误");
        }
        return CommonResult.success(list);
    }

}

