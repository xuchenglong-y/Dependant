package com.read.controller;


import com.read.pojo.ClassTable;
import com.read.pojo.Voice;
import com.read.service.VoiceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/voice")
@Api(tags = "我的_我的语音__郭秉航")
public class VoiceConteoller {
    @Resource
    private VoiceService voiceService;

    @ApiOperation("查询入学测试")
    @GetMapping("/type1")
    public List<Voice> findtype1() {
        return voiceService.findtype1();
    }

    @ApiOperation("查询名师陪练")
    @GetMapping("/type2")
    public List<Voice> findtype2() {
        return voiceService.findtype2();
    }

    @ApiOperation("查询课文陪读")
    @GetMapping("/type3")
    public List<Voice> findtype3() {
        return voiceService.findtype3();
    }

    @ApiOperation("根据v_id删除语音")
    @GetMapping("/delete/{vId}")
    public Boolean delete(@PathVariable Integer vId) {
        return voiceService.delete(vId);
    }
}
