package com.read.controller;

import com.read.common.R;
import com.read.service.AttentionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xcl
 * @version 1.0x
 * @date 2023/1/2 16:57
 */
@RestController
@RequestMapping("/user")
@Api(tags = "视频_关注_徐成龙")
public class AttentionController {

    @Autowired
    private AttentionService attentionService;

    /**
     * 关注：分析业务需求
     *  关注和取消关注
     *  首先查询是否已经关注(当前用户进入其他用户主页  会发送一个请求 来查询是否关注了该用户(如果已经关注了就在redis中查询))
     *  判断是关注还是取消关注 通过前端的true和false来进行  如果是取消关注就在redis中删除该用户
     *  如果是关注 就在中间表中插入数据 已关注者为集合名字 进行创建redis中的set 的集合 关注成功后将关注者id放入redid中
     */
//    @GetMapping("/findYesAndNoAttentionUser")
//    @ApiOperation(value = "")
//    public R attention(){
//        // public Result follow(Long followUserId, Boolean isFollow)
//
//    }
}
