package com.read.controller;


import com.read.pojo.MessageTable;
import com.read.service.MessageTableService;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author xcl
 * @since 2022-12-10
 */
@RestController
@RequestMapping("/msgtable")
@Api(tags = "我的__消息中心__郭秉航")
public class MessageTableController {

    @Resource
    private MessageTableService messageTableService;

    @ApiOperation("查询全部消息")
    @GetMapping("/findList")
    public List<MessageTable> findAll() {
        return messageTableService.findAll();
    }

    @ApiOperation("消除未读")
    @PostMapping("/update")
    public String update(MessageTable messageTable) {
        int result = messageTableService.update(messageTable);
        System.out.println(result);
        if (result >= 1) {
            return "更新成功";
        } else {
            return "更新失败";
        }
    }

}

