package com.read.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.read.common.CommonResult;
import com.read.mapper.JournalismMapper;
import com.read.pojo.Journalism;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xcl
 * @since 2022-12-18
 */
@RestController
@RequestMapping("/journalism")
@Api(tags = "首页_发现_李继承")
public class JournalismController {

    @Resource
    private JournalismMapper journalismMapper;


    @GetMapping("findAll")
    @ApiOperation("查询全部") //测试完毕
    public CommonResult findAll(){
        List<Journalism> list = journalismMapper.selectList(null);
        if (list == null){
            return CommonResult.failed("查询有误");
        }
        return CommonResult.success(list);
    }

    @GetMapping("findName")
    @ApiOperation("根据新闻进行模糊查询")
    @ApiImplicitParam(name = "newsName",value ="新闻标题",required = true)
    public CommonResult findName(@RequestParam(value = "newsName") String newsName){

        QueryWrapper<Journalism> newName1 = new QueryWrapper<Journalism>().like("newsName", "%" + newsName + "%");
        List<Journalism> list =journalismMapper.selectList(newName1);

        return CommonResult.success(list);
    }


}

