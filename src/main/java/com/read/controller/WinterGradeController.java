package com.read.controller;

import com.read.pojo.WinterGrade;
import com.read.service.WinterGradeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/winter")
@Api(tags = "首页_寒假特训__郭秉航")
public class WinterGradeController {
    @Resource
    private WinterGradeService winterGradeService;

    @ApiOperation("查询课程内容")
    @GetMapping("/findAll")
    public List<WinterGrade> findAll(){
        return winterGradeService.findAll();
    }
}
