package com.read.controller;

import com.read.pojo.Teacher;
import com.read.service.TeachersService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/teachers")
@Api(tags = "首页_名师推荐__郭秉航")
public class TeachersController {
    @Resource
    private TeachersService teachersService;

    @ApiOperation("查询全部名师")
    @GetMapping("/findAll")
    public List<Teacher> findAll() {
        return teachersService.findAll();
    }
}
