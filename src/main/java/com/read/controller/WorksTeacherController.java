package com.read.controller;

import com.read.common.R;
import com.read.pojo.WorksTeacher;
import com.read.service.WorksTeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("worksteacher")
@Api(tags = "首页_师生作品_王正智")
public class WorksTeacherController {

    @Autowired
    private WorksTeacherService worksTeacherService;

    @GetMapping("findteacher")
    @ApiOperation("老师音频")
    public List findworksteacher(){

        List<WorksTeacher> t = worksTeacherService.findbytors("t");

        return t;

    }

    @GetMapping("findstudent")
    @ApiOperation("学生音频")
    public List findworksstudent(){

        List<WorksTeacher> s = worksTeacherService.findbytors("s");

        return s;

    }


}
