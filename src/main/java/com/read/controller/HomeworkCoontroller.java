package com.read.controller;

import com.read.pojo.Homework;
import com.read.service.HomeworkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/homework")
@Api(tags = "我的_我的作业__郭秉航")
public class HomeworkCoontroller {
    @Resource
    private HomeworkService homeworkService;

    @ApiOperation("查询作业日期")
    @GetMapping("/homeworktime")
    public List<Homework> fingBytime() {
        return homeworkService.findBytime();
    }

    @ApiOperation("查询作业全部信息")
    @GetMapping("/homeworkAll")
    public List<Homework> fingAll() {
        return homeworkService.findAll();
    }
}
