package com.read.controller;

import com.read.pojo.ClassTable;
import com.read.service.ClassService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/class")
@Api(tags = "我的_我的课时__郭秉航")
public class ClassController {
    @Resource
    private ClassService classService;

    @ApiOperation("查询全部记录")
    @GetMapping("/classList")
    public List<ClassTable> findClass(){
        return classService.findClass();
    }

    @ApiOperation("查询消课课时")
    @GetMapping("/classtype")
    public List<ClassTable> findtype(){
        return classService.findtype();
    }

    @ApiOperation("查询已购课时")
    @GetMapping("/classtypes")
    public List<ClassTable> findtypes(){
        return classService.findtypes();
    }
}
