package com.read.controller;


import com.read.common.CommonResult;
import com.read.mapper.WorksMapper;
import com.read.pojo.Works;
import com.read.service.WorksService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
@RestController
@RequestMapping("works")
@Api(tags = "我的作品_视频_音频_李继承")
public class WorksController {
    @Resource
    private WorksService worksService;


//    @ApiOperation("查询作品url: works/findWorks? userId=1&worksType=1&worksState=1 代表 已通过音频接口"+
//    "  works/findWorks? userId=1&worksType=1&worksState=2 代表 待审核音频借口"+
//    "   works/findWorks? userId=1&worksType=1&worksState=3 代表 未通过音频借口")
    @GetMapping("findWorks")
    public CommonResult findWorks(Integer userId , Integer  worksType, Integer worksState){
        List<Works> works = worksService.findWorks(userId, worksType, worksState);

        return CommonResult.success(works);
    }



}

