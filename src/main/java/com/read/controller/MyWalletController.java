package com.read.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.read.common.R;
import com.read.pojo.MyWallet;
import com.read.service.MyWalletService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


/**
 * @author xcl
 * @email 2364639157@qq.com
 * @date 2022-12-10 16:57:43
 */
@RestController
@RequestMapping("read/myWallet")
@Slf4j
@Api(tags = "我的_我的钱包_徐成龙")
public class MyWalletController {

    @Autowired
    private MyWalletService myWalletService;

    @GetMapping("/findAllMyWalletByUserId")
    @ApiOperation("全部_查询个人钱包全部信息") // 测试完毕
    @ApiImplicitParam(name = "userId",value = "用户ID",required = true)
    public R findAll(@RequestParam(value = "userId") Integer userId) {
        QueryWrapper<MyWallet> myWalletQueryWrapper = new QueryWrapper<>();
        myWalletQueryWrapper.eq("user_id",userId);

        List<MyWallet> myWalletList = myWalletService.list(myWalletQueryWrapper);
        if (myWalletList != null && myWalletList.size() >0) {
            return R.ok().data("myWalletEntities", myWalletList);
        } else {
            return R.error();
        }
    }

    @GetMapping("/findBalanceByUserId")
    @ApiOperation("充值记录") //测试完毕
    @ApiImplicitParam(name = "userId",value = "用户ID",required = true)
    public R findBalance(@RequestParam(value = "userId") Integer userId) {
        if (userId != null && userId > 0) {
            QueryWrapper<MyWallet> myWalletEntityQueryWrapper = new QueryWrapper<>();
            myWalletEntityQueryWrapper.eq("user_id",userId);
            List<MyWallet> myWalletEntityList = myWalletService.list(myWalletEntityQueryWrapper);
            if (myWalletEntityList != null && myWalletEntityList.size() >0) {
                return R.ok().data("wallet_balance", myWalletEntityList);
            } else {
                return R.error();
            }
        } else {
            return R.error();
        }
    }

    @GetMapping("/findRechargeByUserId")
    @ApiOperation("支付记录")//测试完毕
    @ApiImplicitParam(name = "userId",value = "用户ID",required = true)
    public R findRecharge(@RequestParam(value = "userId") Integer userId) {
        QueryWrapper<MyWallet> myWalletEntityQueryWrapper = new QueryWrapper<>();
        myWalletEntityQueryWrapper.select("wallet_pay")
                .eq("user_id", userId);

        List<MyWallet> myWalletList = myWalletService.list(myWalletEntityQueryWrapper);
        if (myWalletList != null && myWalletList.size() >0) {
            return R.ok().data("myWalletList", myWalletList);
        } else {
            return R.error();
        }
    }

    @GetMapping("/findWalletBalanceByUserId") //余额
    @ApiOperation("余额") //
    @ApiImplicitParam(name = "userId",value = "用户ID",required = true)
    public R findWalletbalance(@RequestParam(value = "userId") Integer userId) {
        QueryWrapper<MyWallet> myWalletEntityQueryWrapper = new QueryWrapper<>();
        myWalletEntityQueryWrapper.eq("user_id", userId)
                .select("wallet_balance");
        List<MyWallet> list = myWalletService.list(myWalletEntityQueryWrapper);
        if (list != null && list.size()>0) {
            return R.ok().data("walletbalance", list);
        }else {
            return R.error();
        }
    }

}
