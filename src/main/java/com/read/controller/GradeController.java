package com.read.controller;

import com.read.pojo.GradeTable;
import com.read.service.GradeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/grade")
@Api(tags = "我的_我的班级__郭秉航")
public class GradeController {
    @Resource
    private GradeService gradeService;

    @ApiOperation("查询全部记录")
    @GetMapping("/gradeList")
    public List<GradeTable> findAll(){
        return gradeService.findAll();
    }
}
