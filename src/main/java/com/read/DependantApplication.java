package com.read;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.read.mapper")
public class DependantApplication {
    public static void main(String[] args) {
        SpringApplication.run(DependantApplication.class, args);
    }

}

