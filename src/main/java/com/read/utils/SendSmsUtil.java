package com.read.utils;

import com.alibaba.fastjson.JSON;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.tea.TeaException;

import java.util.HashMap;

/**
 * @date 2022/9/6
 * @desc
 */
public class SendSmsUtil {
    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                // 您的 AccessKey ID
                .setAccessKeyId("accessKeyId")
                // 您的 AccessKey Secret
                .setAccessKeySecret("accessKeySecret");
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }
    public static String sendCheckCode(String phone){
        com.aliyun.dysmsapi20170525.Client client = null;
        try {
            client = SmsSample.createClient("LTAI5tCExamQRV8pTg65HpRL", "tVkVa8zq2BL5g02mKLFCTgtowNbWJj");
        } catch (Exception e) {
            e.printStackTrace();
        }
        com.aliyun.dysmsapi20170525.models.SendSmsRequest sendSmsRequest = new com.aliyun.dysmsapi20170525.models.SendSmsRequest()
                .setPhoneNumbers(phone)
                .setSignName("tongtong")
                .setTemplateCode("SMS_264945135");
        //.setTemplateParam("{\"code\":\"666666\"}");

        HashMap<String, String> hashMap = new HashMap<>();
        //  随机 6 位 整数验证码
        String code = (int)(Math.random()*900000 + 100000) + "";
        hashMap.put("code",code);
        String param = JSON.toJSONString(hashMap);
        sendSmsRequest.setTemplateParam(param);

        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        try {
            // 复制代码运行请自行打印 API 的返回值
            SendSmsResponse sendSmsResponse = client.sendSmsWithOptions(sendSmsRequest, runtime);
            return code;
        } catch (TeaException error) {
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        }
        return "异常了";
    }
}
