package com.read.utils;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;

import java.util.ArrayList;
import java.util.List;


public class MybatisPlusGenerator {
    public static void main(String[] args) {
        //代码生成对象
        AutoGenerator mpg = new AutoGenerator();
        //默认模拟引擎 Veloctiy
        mpg.setTemplateEngine(new VelocityTemplateEngine());

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        //开发人员名称
        gc.setAuthor("Java_myj");
        //代码生成目录
        gc.setOutputDir("D:\\IDEAJC\\IDEAXXOO\\Dependant\\src\\main\\java");
        // 是否覆盖同名文件，默认是false
        gc.setFileOverride(true);
        // XML 二级缓存
        gc.setEnableCache(false);
        // XML ResultMap
        gc.setBaseResultMap(true);
        // XML columList
        gc.setBaseColumnList(true);
        //去掉Service的前缀I
        gc.setServiceName("%sService");
        //实体属性 Swagger2 注解，提前导入 Swagger 依赖
        gc.setSwagger2(true);
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUsername("Read");
        dsc.setPassword("123456");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUrl("jdbc:mysql://47.92.214.180:3306/read?useUnicode=true&characterEncoding=utf8&serverTimezone=Asia/Shanghai");
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        //父类包名，和创建项目时起的包名一致
        pc.setParent("com.read");
        //pc.setModuleName("user");//在父类包下再创建一个包（可以设置不同模块生成不同包）
        //手动设置包名
        //实体类包名
        pc.setEntity("pojo");
        //mapper接口包名
        pc.setMapper("mapper");
        //mapper.xml文件包名
        pc.setXml("mappers");
        mpg.setPackageInfo(pc);


        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        //数据库表映射到实体的命名策略:下划线转驼峰
        strategy.setNaming(NamingStrategy.underline_to_camel);
        //数据库表字段映射到实体的命名策略, 未指定按照 naming 执行
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        //实体是否为lombok模型
        strategy.setEntityLombokModel(true);
        //生成 @RestController 控制器
        strategy.setRestControllerStyle(true);
        //驼峰转连字符
        strategy.setControllerMappingHyphenStyle(true);
        //strategy.setTablePrefix("t_");//生成代码时类名不带表前缀
        //手动指定映射表名（不写参数映射所有表）
        //通过表名对当前表生成代码
        strategy.setInclude("conllect");
        //生成以t_开头的多张表的代码
        //strategy.setLikeTable(new LikeTable("t_"));
        mpg.setStrategy(strategy);

        //解决Mapper.xml生成位置
        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        // String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        String templatePath = "/templates/mapper.xml.vm";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名以及位置 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return "D:\\IDEAJC\\IDEAXXOO\\Dependant\\src\\main\\/resources/mappers/"
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        /*
        cfg.setFileCreate(new IFileCreate() {
            @Override
            public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
                // 判断自定义文件夹是否需要创建
                checkDir("调用默认方法创建的目录，自定义目录用");
                if (fileType == FileType.MAPPER) {
                    // 已经生成 mapper 文件判断存在，不想重新生成返回 false
                    return !new File(filePath).exists();
                }
                // 允许生成模板文件
                return true;
            }
        });
        */
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
        // templateConfig.setEntity("templates/entity2.java");
        // templateConfig.setService();
        // templateConfig.setController();
        // 不生成系统配置的xml文件
        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // 执行生成
        mpg.execute();

    }
}