// This file is auto-generated, don't edit it. Thanks.
package com.read.utils;

import com.alibaba.fastjson.JSON;
import com.aliyun.tea.*;

import java.util.HashMap;
// 阿里云sms   验证码demo
public class SmsSample {

    /**
     * 使用AK&SK初始化账号Client
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.dysmsapi20170525.Client createClient(String accessKeyId, String accessKeySecret) throws Exception {
        com.aliyun.teaopenapi.models.Config config = new com.aliyun.teaopenapi.models.Config()
                // 您的 AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的 AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        return new com.aliyun.dysmsapi20170525.Client(config);
    }

    public static void main(String[] args_) throws Exception {
        java.util.List<String> args = java.util.Arrays.asList(args_);
        com.aliyun.dysmsapi20170525.Client client = SmsSample.createClient("LTAI5tFSQGFwMiB64cdSM4zX", "smFotAZBxUWB1LwbJ5Go8ZCY1q9ykb");
        com.aliyun.dysmsapi20170525.models.SendSmsRequest sendSmsRequest = new com.aliyun.dysmsapi20170525.models.SendSmsRequest()
                .setPhoneNumbers("15538586250")
                .setSignName("ABC商城")   // 签名
                .setTemplateCode("SMS_200702765");  //模板code
                //.setTemplateParam("{\"code\":\"666666\"}");

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("code","666666");

        String param = JSON.toJSONString(hashMap);
        sendSmsRequest.setTemplateParam(param);

        com.aliyun.teautil.models.RuntimeOptions runtime = new com.aliyun.teautil.models.RuntimeOptions();
        try {
            // 复制代码运行请自行打印 API 的返回值
            client.sendSmsWithOptions(sendSmsRequest, runtime);
        } catch (TeaException error) {
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        } catch (Exception _error) {
            TeaException error = new TeaException(_error.getMessage(), _error);
            // 如有需要，请打印 error
            com.aliyun.teautil.Common.assertAsString(error.message);
        }
    }
}