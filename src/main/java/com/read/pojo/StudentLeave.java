package com.read.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author xcl
 * @since 2022-12-11
 */
@Data
@ApiModel("学生请假对象")
public class StudentLeave {

    private static final long serialVersionUID = 1L;

    /**
     * 学生ID
     */
    @ApiModelProperty("学生ID")
    @TableId(value = "student_id", type = IdType.AUTO)
    private Integer studentId;

    /**
     * 学生所在班级
     */
    @ApiModelProperty("学生所在班级")
    private String studentClass;

    /**
     * 请假类型 病假 事假 其他
     */
    @ApiModelProperty("请假类型 病假 事假 其他")
    private String studentType;

    /**
     * 请假开始时间
     */
    @ApiModelProperty("请假开始时间")
    private LocalDateTime startTime;

    /**
     * 请假结束时间
     */
    @ApiModelProperty("请假结束时间")
    private LocalDateTime endTime;

    /**
     * 备注原因
     */
    @ApiModelProperty("备注原因")
    private String info;

    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    private Integer userId;

}
