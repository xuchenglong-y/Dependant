package com.read.pojo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel("名师陪练对象 ")
@AllArgsConstructor
@NoArgsConstructor
public class Exercise {
    private Integer id;
    private String Title;
    private String Voice;


}
