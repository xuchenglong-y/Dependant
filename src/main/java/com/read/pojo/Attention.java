package com.read.pojo;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author xcl
 * @version 1.0x
 * @date 2023/1/2 17:01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("对象关注")
public class Attention {
    /**
     * 关联表Id
     */
    private Integer Id;

    /**
     * 用户Id
     */
    private Integer userId;

    /**
     * 关联表Id
     */
    private Integer followUserId;

    /**
     * 关注时间
     */
    private Date createTime;
}
