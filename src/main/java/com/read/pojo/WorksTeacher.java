package com.read.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("works_teacher")
@ApiModel("师生作品")
public class WorksTeacher {

    private int id;//id

    private String name;//昵称

    private int quantity;//播放量

    private String cla;//分类

    private String author;//作者

    private String tors;//老师或学生

    private String pic;//头像
}
