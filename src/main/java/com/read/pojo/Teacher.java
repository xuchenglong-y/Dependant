package com.read.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class Teacher {
    private Integer tId;
    private String tName;
    private String tTitle;
    private String tText;

    @TableField("t_stu_num")
    private Integer tNum;
    private Float good_review;
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer gettId() {
        return tId;
    }

    public void settId(Integer tId) {
        this.tId = tId;
    }

    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    public String gettTitle() {
        return tTitle;
    }

    public void settTitle(String tTiele) {
        this.tTitle = tTiele;
    }

    public String gettText() {
        return tText;
    }

    public void settText(String tText) {
        this.tText = tText;
    }

    public Integer gettNum() {
        return tNum;
    }
}

