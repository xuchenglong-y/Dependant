package com.read.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("作品对象")
public class Works implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 作品id
     */
    @TableId(value = "works_id", type = IdType.AUTO)
    private Integer worksId;

    /**
     * 作品名称
     */
    private String worksName;

    /**
     * 作品作者
     */
    private String worksAuthor;

    /**
     * 作品简介
     */
    private String worksIntroduction;

    /**
     * 作品地址
     */
    private String worksAddress;

    /**
     * 作品类型（1.音频 2.视频）
     */
    private Integer worksType;

    /**
     * 作品状态（1.已通过 2.审核中 3.未通过）
     */
    private Integer worksState;


}
