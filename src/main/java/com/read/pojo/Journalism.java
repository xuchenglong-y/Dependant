package com.read.pojo;

import java.time.LocalDate;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xcl
 * @since 2022-12-18
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("发现对象")
public class Journalism implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer newid;

    private String newsname;

    private String newscontent;

    private String newaddress;

    private LocalDate newscreatetime;

    private Integer newscomments;

    private Integer newslike;

    private Integer newscollection;


}
