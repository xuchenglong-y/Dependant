package com.read.pojo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("班级对象")
public class GradeTable {
    private Integer gId;
    private String gName;
    private Integer gAge;
    private String gSex;

    public Integer getgId() {
        return gId;
    }

    public void setgId(Integer gId) {
        this.gId = gId;
    }

    public String getgName() {
        return gName;
    }

    public void setgName(String gName) {
        this.gName = gName;
    }

    public Integer getgAge() {
        return gAge;
    }

    public void setgAge(Integer gAge) {
        this.gAge = gAge;
    }

    public String getgSex() {
        return gSex;
    }

    public void setgSex(String gSex) {
        this.gSex = gSex;
    }

    public GradeTable() {
    }
}
