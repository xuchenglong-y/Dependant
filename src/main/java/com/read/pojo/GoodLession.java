package com.read.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xcl
 * @since 2022-12-15
 */
@Data
@ApiModel("网课对象")
public class GoodLession implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 精选好课id
     */
    @TableId(value = "g_id", type = IdType.AUTO)
    private Integer gId;

    /**
     * 课程名字
     */
    private String gName;

    /**
     * 课程价格
     */
    private Integer gPrice;

    /**
     * 报名人数
     */
    private Integer gNum;


}
