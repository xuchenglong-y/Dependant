package com.read.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ApiModel("作业对象")
public class Homework {
    private Integer hId;
    private String hName;
    private String hText;

    @JsonFormat(pattern = "yyyy-MM-dd ", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd ")
    private Date hCreate;

    public Integer gethId() {
        return hId;
    }

    public void sethId(Integer hId) {
        this.hId = hId;
    }

    public String gethName() {
        return hName;
    }

    public void sethName(String hName) {
        this.hName = hName;
    }

    public String gethText() {
        return hText;
    }

    public void sethText(String hText) {
        this.hText = hText;
    }

    public Date gethCreate() {
        return hCreate;
    }

    public void sethCreate(Date hCreate) {
        this.hCreate = hCreate;
    }

    public Homework() {
    }
}
