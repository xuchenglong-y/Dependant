package com.read.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xcl
 * @since 2022-12-13
 */
@Data
@TableName("sys_vedios")
@ApiModel("音频视频上传对象")
public class Vedios {

    private static final long serialVersionUID = 1L;

    /**
     * 视频ID
     */
    @TableId(value = "video_id", type = IdType.AUTO)
    @ApiModelProperty("视频ID")
    private Integer videoId;

    /**
     * 名称
     */
    @ApiModelProperty("名称")
    private String videoName;

    /**
     * 演员
     */
    @ApiModelProperty("演员")
    private String videoPerfermers;

    /**
     * 校区
     */
    @ApiModelProperty("校区")
    private String school;

    /**
     * 简介
     */
    @ApiModelProperty("简介")
    private String introduction;

    /**
     * 音视频链接地址
     */
    @ApiModelProperty("音视频链接地址")
    private String videoAddress;

    /**
     * 上传时间
     */
    @ApiModelProperty("上传时间")
    private LocalDateTime createTime;

    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    private Integer userId;

    /**
     * 1为音频 2为视频
     */
    private Integer videoStates;

    /**
     *1 为审核中 2 为 已通过 3未通过
     */
    private Integer videoType;
}
