package com.read.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@ApiModel("课时对象")
public class ClassTable {
    private Integer cId;
    private String cName;
    private String cText;
    private Integer cType;
    private Integer cNum;


    @JsonFormat(pattern = "yyyy-MM-dd ", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd ")
    private Date class_time;

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getcText() {
        return cText;
    }

    public void setcText(String cText) {this.cText = cText;}

    public Integer getcType() {
        return cType;
    }

    public void setcType(Integer cType) {
        this.cType = cType;
    }

    public Integer getcNum() {
        return cNum;
    }

    public void setcNum(Integer cNum) {
        this.cNum = cNum;
    }

    public Date getClass_time() {
        return class_time;
    }

    public void setClass_time(Date class_time) {
        this.class_time = class_time;
    }

    public ClassTable() {
    }

}
