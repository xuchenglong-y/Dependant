package com.read.pojo;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("user_materials")
@ApiModel("用户资料")
public class UserMmaterials {

    private String userId;//id

    private String name;//昵称

    private int sex;//性别

    private String mobile;//电话号，账号

    private String password;//登录密码
    @TableLogic
    private Integer del;//逻辑删除

    private Integer version;//乐观锁

    private String avatar;//头像

    private String payment;//支付密码




}
