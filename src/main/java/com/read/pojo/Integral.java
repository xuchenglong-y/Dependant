package com.read.pojo;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xcl
 * @since 2022-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("积分对象")
public class Integral implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer integralId;

    private String integralCount;

    private String integralSchool;

    private String integralCountry;


}
