package com.read.pojo;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel("寒假特训班对象")
public class WinterGrade {
    private Integer wId;
    private String wType;
    private String wName;
    private String wNum;
    private String image;

    public Integer getwId() {
        return wId;
    }

    public void setwId(Integer wId) {
        this.wId = wId;
    }

    public String getwType() {
        return wType;
    }

    public void setwType(String wType) {
        this.wType = wType;
    }

    public String getwName() {
        return wName;
    }

    public void setwName(String wName) {
        this.wName = wName;
    }

    public String getwNum() {
        return wNum;
    }

    public void setwNum(String wNum) {
        this.wNum = wNum;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public WinterGrade() {
    }
}
