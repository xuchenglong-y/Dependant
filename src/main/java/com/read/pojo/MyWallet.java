package com.read.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author xcl
 * @email 2364639157@qq.com
 * @date 2022-12-10 16:57:43
 */
@Data
@TableName("read_my_wallet")
@ApiModel("钱包对象")
public class MyWallet implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 钱包ID
     */
    @TableId
    @ApiModelProperty("钱包ID")
    private Integer walletId;
    /**
     * 钱包余额
     */
    @ApiModelProperty("钱包余额")
    private String walletBalance;
    /**
     * 钱包充值
     */
    @ApiModelProperty("钱包充值")
    private String walletRecharge;
    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date createTime;
    /**
     * 版本
     */
    @ApiModelProperty("版本")
    private String version;
    /**
     * 钱包支付
     */
    @ApiModelProperty("钱包支付")
    private String walletPay;
    /**
     * 用户ID
     */
    @ApiModelProperty("用户ID")
    private Integer UserId;
}
