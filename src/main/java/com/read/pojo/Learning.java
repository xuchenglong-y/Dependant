package com.read.pojo;

import java.time.LocalDateTime;
import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author xcl
 * @since 2022-12-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("学习记录对象")
public class Learning implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学习记录id
     */
    private Integer learningId;

    /**
     * 日期
     */
    private LocalDateTime learningDate;

    /**
     * 时间
     */
    private Integer learningTime;


}
