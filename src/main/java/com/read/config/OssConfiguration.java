package com.read.config;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 把配置文件中的配置信息读取到该类中.
 */
@Data
@Configuration
public class OssConfiguration {

    @Value("${endpoint}")
    private String endPoint;

    @Value("${accessKeyId}")
    private String accessKeyId;

    @Value("${accessKeySecret}")
    private String accessKeySecret;

    @Value("${filehost}")
    private String fileHost;

    @Value("${bucketName}")
    private String bucketName;

}