package com.read.config;

import java.io.FileWriter;
import java.io.IOException;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *修改日期：2017-04-05
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
 */

public class AlipayConfig {
	
//↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

	// 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
	public static String app_id = "2021000121699610";
	
	// 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCricPFWKLisXZ2ze4P7Nu0zwkzqN4RAh9FzhcIuDox3lvcMNvmMYI5w3fA1iTKiiXHgsUoEEyNWMVxNJsD5UeDW7Q/5t81EA7Oi8FhrTXyZ3xiRXJwHSacnTJ95Joiih1cMQ3j0OUSyCAW0urBKmF3LUFqBBBHGOHaqUzG2ococ4yprEqIF135zHrVA1xf921hPQm1a4zZONw39pOCdKYe8iXpSXppgzxuC6fvGuQgtg1mnb2kLwDaepyekKoZlR5740WtO/v7oUDAH9GCIVACtOTK/lxUsbPQ6TdpfYkCUsm9GfuE4O9ubvy+MGHrTUTP2adshHPwWPFn6ZYA6n6RAgMBAAECggEBAJfxsNKmv6lzrk5v/JZopPhUh38V7pY8Pf00Hx0fKpUvtEinkrElW4fVB8dtKCusEOjXgdIKFfUCE+pBwanCtwS4/0G3PDGhZqw4CPR/XBC67+eQQlzixWfmqmnvS0Sa+ITIAtiwGaj4UcxFyaGO68SH9rB0pwKDHNP2MMbXnO8LbKe2hWwfkIYx2AziGP/a4iHG52OqJsrqAcLMZYtRX+E42JEpRJ15WLFzzg1202pU0gXb+O0mfHcYIBBEp9/xmuZm0sSS6NUN056hpnQ7HNhYEQVwZ1E0o6D7E9TxgvhSpw5ZJiW3q5bG2wPtVz06fHr38GWfnunhMqrcHPb3Dh0CgYEA7y7xmCG8LsF3Oe202sfqe9I2xr0T6YDKgRmtNZnOfrEpg3ehae00NKLZwAAZVhGNSBZqgxb8itCXbFSOc40hPOZd7JS9oG7N3ohZiQw+NJGLcEzS1cZLr0Ihy9/eMSHpHjukKbt3crr+etuWSA5Xb2uUiwQCAc3GUYtsSlc07sMCgYEAt5lGp10GXZ4qSEBtDeHkBQaKtVESZV5B6Opj16r1hvePPGuo5Pp+hANbgKOUVKZ2YRlwE+KG881sxvzGTcKB3Z+6BhKm4Of2aJ1Y0Lo3XfsydD5g8+thu2qi+RLgCF74XmTX6DP01++KTmjAAOaGz5GO1ZZA5z85NgTGZSSgcBsCgYEA0y0oY8CNTN7FrKDCiFPwJMjSUiAIn6Z1pQyNkICu/87dr1SFcGfdKwgJp2qu8/3lNpuMCRfXe5x1xVDWwRD/IqzmioY88pRCMdNC8iycE3/ZlBpQICzHEAMcocuTet/M2kOgSrRY3zRYkRKlmhMHgbmDWkX8m6kaC09FGlHqy08CgYB6x3rzY4dlRed6x/LmDMFDBxvto1jNB1rGWzEHwiFN7/IO8uRQtJ+0wtYHUd1ZOrUBvFQySBP9qMvsypAG4mOchTuO4hxNXXJwhYEmnNF3/QvBpxGJLUj/X5G7RX8iufgYkBG/DDfZzCxOwUhdkcxntSyy8zdWLfahhFH0w6izLwKBgGnmtYMfJq6cL7EB0gS04ui0EjthWUEZDPAuLa4tC16JYAXdRJXIDPY5CVENwno6ieBQ7cffd/WuRyggNhyzpJAIWfm3yRv9gJ9U6+z2+DXbUJzGAmg8SnN632CiY78r/n5Dk1tYDWs63NBO79KwbvaWI/Orgxah6rKjB083Y5CL";
	
	// 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnNimykgajnGv90e0PrVr+UEkK5t0VZ9Tukw4SQexEWsrFnwxhjs3wSfni7i32A1uz2r1BfJrPWRpIIceCAsJW6c87n47Ny0T+0tDnxx1S/mJI47XXgisZ59lOyWMs4Iy+Mjswu+bHcKAXUyP6BftEFsW8dr69AQ1IZprPT24chUMji+0wGdYwxv+KIsKpiN/v1GxEGTWG/c4WYM3QhrNYXqrRCDCurpuQ0xRoVOVUl6igfcMUbJrH0baY7HSElS07mrT1bIWEVu7DxAOFhA4H4vtJLZxsECBZLwGdi2lb6qmkyP/ClKCmCryGVRFVHttcXC3U9c0kZUymMZLlTzz+wIDAQAB";

	// 服务器异步通知页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String notify_url = "http://localhost:8080/alipay.trade.page.pay-JAVA-UTF-8/notify_url.jsp";

	// 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
	public static String return_url = "http://localhost:8080/alipay.trade.page.pay-JAVA-UTF-8/return_url.jsp";

	// 签名方式
	public static String sign_type = "RSA2";
	
	// 字符编码格式
	public static String charset = "utf-8";
	
	// 支付宝网关
	public static String gatewayUrl = "https://openapi.alipay.com/gateway.do";
	
	// 支付宝网关
	public static String log_path = "C:\\";


//↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑

    /** 
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(log_path + "alipay_log_" + System.currentTimeMillis()+".txt");
            writer.write(sWord);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

