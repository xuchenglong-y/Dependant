package com.read.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.injector.ISqlInjector;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @date 2022/10/24
 * @desc
 */
@EnableTransactionManagement
@Configuration
@MapperScan("com.read.mapper")
public class MybatisPlusConfig {

    @Bean
    public MybatisPlusInterceptor optimisticLockerInterceptor() {

        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

        //分页插件
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor(DbType.MYSQL);
        interceptor.addInnerInterceptor(paginationInnerInterceptor);    // 添加分页拦截器  --- 分页插件

        //乐观锁插件
        OptimisticLockerInnerInterceptor optimisticLockerInnerInterceptor = new OptimisticLockerInnerInterceptor();

        interceptor.addInnerInterceptor(optimisticLockerInnerInterceptor);   // 添加乐观锁拦截器 --- 乐观锁插件
        return interceptor;
    }


    @Bean
    public ISqlInjector sqlInjector() {
        // 逻辑删除 会用到
        return new DefaultSqlInjector();
    }


}
