package com.read.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc
public class Knife4jConfiguration {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        .title("筒筒陪读")
                        .description("# 传播文化 赋能未来")
                        .termsOfServiceUrl("http://47.92.214.180:8080/doc.html#/home")
                        .contact(new Contact("徐成龙","http://47.92.214.180:8080/doc.html#/home","2364639157@qq.com"))
                        .version("3.0")
                        .build())
                //分组名称
                .groupName("测试版本")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.read.controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;
    }
}